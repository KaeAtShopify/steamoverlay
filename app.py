from flask import Flask

import requests
import http.client
import json
import time

app = Flask(__name__)

http.client.HTTPConnection.debuglevel = 1

# Jon 76561197960424434
# Kae 76561198042342275

steamId = "76561198042342275"
appId = "462790"
key = "3A1C59BE6ACF6CEF0848E7FAAC2080E3"
orderid = 0

def getUserInfo():
    return requests.get('https://api.steampowered.com/ISteamMicroTxnSandbox/GetUserInfo/V0001/', {'key': key, 'steamid': steamId}).json()

def buildTransaction():
    global orderid
    orderid = int(time.time())
    t = {}
    t['key'] = key
    t['orderid'] = orderid
    t['steamid'] = steamId
    t['appid'] = appId
    t['itemcount'] = 1
    t['language'] = "EN"
    t['currency'] = getUserInfo()['response']['params']['currency']
    t['itemid[0]'] = 1111
    t['qty[0]'] = 1
    t['amount[0]'] = 100
    t['description[0]'] = 'A wicked Shopify object'
    # return json.dumps(t)
    return t

def buildFinalize():
    t = {}
    t['key'] = key
    t['orderid'] = orderid
    t['appid'] = appId
    return t

@app.route('/finalize')
def Finalize():
    print(orderid)
    posted = requests.post('https://api.steampowered.com/ISteamMicroTxnSandbox/FinalizeTxn/V0001/', buildFinalize())
    return posted.text

@app.route('/purchase')
def purchase():
    respond = requests.post('https://api.steampowered.com/ISteamMicroTxnSandbox/InitTxn/V0002/', buildTransaction())

    if respond.status_code == 200:
        if respond.json()['response']['result'] == 'OK':
            #Finalize()
            return "Purchase created"

        elif respond.json()['response']['result'] == 'Failure':
            return "Cannot create transaction: " + str(respond.json()['response']['error']['errordesc'])

    else:
        return respond.text
